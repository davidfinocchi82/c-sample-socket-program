#include <iostream>
#include <queue>
#include <functional>
#include <mutex>
#include "../libs/pugixml-1.9/src/pugixml.hpp"
#include "SocketServer.h"

SocketServer sock;
mutex mtx;

std::queue<std::function<void()>> funcs;
void processXml(const string str);

// template for handling queue functions
template<typename _Callable, typename... _Args>
void QueueFunction(_Callable&& __f, _Args&&... __args)
{
    std::function<void()> func = std::bind(std::forward<_Callable>(__f), std::forward<_Args>(__args)...);
    funcs.push(func);
}

// processes the queue
void CallFuncs()
{
    while(!funcs.empty())
    {
        std::function<void()> func = funcs.front();
        funcs.pop();
        func();
    }
}

// loop that waits for incoming packets
void * loop(void * m)
{
    pthread_detach(pthread_self());
	while(1)
	{
		string str = sock.getMessage();
		if( str != "" )
		{
            QueueFunction(processXml, str);
		}
		usleep(1000);
        CallFuncs();
	}
	sock.detach();
}

// main method
int main(int argc, char *argv[])
{
	pthread_t msg;
    if (argc != 3) {
        sock.setup();
    }
    else {
        sock.setup(argv[1], atoi(argv[2]));
    }
	if( pthread_create(&msg, NULL, loop, (void *)0) == 0)
	{
		sock.receive();
	}
	return 0;
}

// parses the XML packets
void processXml(const string str)
{
    pugi::xml_document doc;
    pugi::xml_parse_result result = doc.load_string(str.c_str());
    if (result) {
        mtx.lock();
        for(auto command : doc.select_nodes("//request/command"))
        {
            cout << command.node().child_value() << endl;
        }
        for(auto row : doc.select_nodes("//request/data/row"))
        {
            cout << row.node().child_value() << endl;
        }
        mtx.unlock();
    }
    else
    {
        sock.Send("Unknown Command");
    }
    sock.Send("<?xml version = '1.0' encoding = 'UTF-8'?> "
        "<response> "
        "<command>Print</command> "
        "<status>Complete</status> "
        "<date>1970-01-01 00:00:00</date> "
        "</response>");
    sock.clean();
}