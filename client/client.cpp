#include <iostream>
#include <signal.h>
#include "SocketClient.h"

SocketClient sock;

// exits socket
void sig_exit(int s)
{
	sock.exit();
	exit(0);
}

// main method
int main(int argc, char *argv[])
{
	signal(SIGINT, sig_exit);
	sock.setup("127.0.0.1",5000);
    string s(sock.readFile("example.xml"));
    srand(time(NULL));
    sock.Send(s);
    string rec = sock.receive();
    if( rec != "" )
    {
        cout << rec << endl;
    }
	return 0;
}