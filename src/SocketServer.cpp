#include "SocketServer.h" 

string SocketServer::Message;

// creates the task for the socket server. 
void* SocketServer::Task(void *arg)
{
	int n;
	int newsockfd = (long)arg;
	char msg[MAXPACKETSIZE];
	pthread_detach(pthread_self());
	while(1)
	{
		n=recv(newsockfd,msg,MAXPACKETSIZE,0);
		if(n==0)
		{
		   close(newsockfd);
		   break;
		}
		msg[n]=0;
		
		Message = string(msg);
        }
	return 0;
}

// sets up the socket server. 
void SocketServer::setup(const char* address, int port)
{
	sockfd=socket(AF_INET,SOCK_STREAM,0);
 	memset(&serverAddress,0,sizeof(serverAddress));
	serverAddress.sin_family=AF_INET;
	serverAddress.sin_addr.s_addr=inet_addr(address);
	serverAddress.sin_port=htons(port);
	bind(sockfd,(struct sockaddr *)&serverAddress, sizeof(serverAddress));
 	listen(sockfd,5);
}

// handles the incoming packet. 
string SocketServer::receive()
{
	string str;
	while(1)
	{
		socklen_t sosize  = sizeof(clientAddress);
		newsockfd = accept(sockfd,(struct sockaddr*)&clientAddress,&sosize);
		str = inet_ntoa(clientAddress.sin_addr);
		pthread_create(&serverThread,NULL,&Task,(void *)newsockfd);
	}
	return str;
}

// gets the packet information
string SocketServer::getMessage()
{
	return Message;
}

// sends a packet to the client. 
void SocketServer::Send(string msg)
{
	send(newsockfd,msg.c_str(),msg.length(),0);
}

// cleans the socket. 
void SocketServer::clean()
{
	Message = "";
	memset(msg, 0, MAXPACKETSIZE);
}

// detaches the socket. 
void SocketServer::detach()
{
	close(sockfd);
	close(newsockfd);
} 