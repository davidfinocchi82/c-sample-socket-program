# C++ Sample Socket Program

The sample socket program is a simple program to showcase my programming abilities with C++. This was written using Visual Studio Code on Ubuntu 18.04.2 LTS.

  - Used Object-Oriented Programming in both the server and the client. 
  - Showcased multiple ways of doing tasks. 
  - Included most of the Extra Credit items.

# Starting the Server 

  - Open up a new terminal and change directory into the server source folder. 
  - Type **make all**. This will compile the server program.
  - After the program compiles then type **./server**.
  - You can add 2 optional command line parameters. IP address and port for the incoming connection. 
  - Example: **./server "127.0.0.1" 10000**. Passing no command line arguments will use the default IP 127.0.0.1 and the port 5000. 

# Starting the Client

  - Open up a new terminal and change directory into the client source folder. 
  - Type **make all**. This will compile the client program
  - After the program compiles then type **./client** after you start the server program listed above. 

# Reasoning behind my design approach

  Advantages of Object-Oriented Programming

  - OOP provides a clear modular structure for programs which makes it good for defining abstract datatypes where implementation details are hidden and the unit has a clearly defined interface.

  - OOP makes it easy to maintain and modify existing code as new objects can be created with small differences to existing ones.

  - OOP provides a good framework for code libraries where supplied software components can be easily adapted and modified by the programmer. This is particularly useful for developing graphical user interfaces.

  Advantages of Using Templates

  - C++ templates provide a way to re-use source code as opposed to inheritance and composition which provide a way to re-use object code. 
  
  - C++ provides two kinds of templates: class templates and function templates. Use function templates to write generic functions that can be used with arbitrary types.

  Advantages and Disadvantages of Using 3rd Party Libraries

  Pros: 

  - (Usually) Very fast to setup and hook into your code. Since I had limited time I could not have written an XML parser. Therefore, I used a well known library. 

  - Reasonable guarantees that the functions will work as advertised

  - Reasonable guarantees that the code will run quickly.

  Cons: 

  - Adding functionality or making specific tweaks can be difficult (or impossible) and would require you to go in-depth to learn the implementation of the library.

  - Sometimes you're 'black boxed' from the actual implementation.

  - You're placing a lot of trust in the library's dev, and in certain situations that they continue to maintain their library.
